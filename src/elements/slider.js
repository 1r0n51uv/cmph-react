import React from 'react'
import firebase from 'firebase/app';

class Slider extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            slide1: '',
            slide2: '',
            slide3: '',
        };
        this.renderSlider();
    }

    renderSlider() {
        firebase.firestore().collection('slider').doc('5RJBL8zvuB9pYLT6lXDs').get().then(value => {
            this.setState({slide1: value.data()['path']});
        });

        firebase.firestore().collection('slider').doc('W8fDD3dcKQIc7lZamVL7').get().then(value => {
            this.setState({slide2: value.data()['path']});
        });

        firebase.firestore().collection('slider').doc('uT3GZ1wipEAB8v2puPOe').get().then(value => {
            this.setState({slide3: value.data()['path']});
        });
    }


    render() {




        return(
            <div>
                <section className="banner-area relative" id="home">
                <div className="slider"><div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">


                    <div className="carousel-inner" role="listbox">


                        <div className="carousel-item active" style={{backgroundImage: `url(${this.state.slide1})`}} />
                        <div className="carousel-item" style={{backgroundImage: `url(${this.state.slide2})`}} />
                        <div className="carousel-item" style={{backgroundImage: `url(${this.state.slide3})`}} />


                    </div>


                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"/>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"/>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
                </div>
            </section>

            </div>
        );
    }


}

export default  Slider;