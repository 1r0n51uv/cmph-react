import React from 'react'
import logo from './../assets/logo2.png'

class Navbar extends React.Component {


    render() {

        return(
            <div>
                <header className="default-header" >
                    <div className="container">
                        <div className="header-wrap">


                            <div className="header-top d-flex justify-content-between align-items-center">

                                <div className="logo" style={{marginLeft: '45%'}}>
                                    <a href="home"><img src={logo} alt=""/></a>
                                </div>

                                <div className="main-menubar d-flex align-items-center">
                                    <nav className="hide">
                                        <a href="home">Home</a>
                                        <a href="about">About Me</a>
                                        <a href="gallery">Gallery</a>
                                        <a href="blog">Blog</a>
                                    </nav>
                                    <div className="menu-bar"><i style={{color: 'white', fontSize: '150%'}} className="fas fa-camera"/></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </header>
            </div>
        );

    }

}

export default Navbar;