import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                <footer className="footer-area">
                    <div className="container">
                        <div className="row footer-bottom d-flex justify-content-between">
                            <p className="col-lg-8 col-sm-12 footer-text m-0 text-white">Copyright &copy;
                                All rights reserved | This template is made with <i className="fa fa-heart"
                                                                                    aria-hidden="true"/> by <a
                                    href="https://colorlib.com">Colorlib</a> and developed by <a href="https://colorlib.com">1r0n51uv</a></p>

                            <div className="col-lg-4 col-sm-12 footer-social">
                                <a href="https://www.facebook.com/caterinamarcianophotographer/"><i className="fab fa-facebook-square" style={{fontSize: '120%'}}/></a>
                                <a href="https://www.instagram.com/caterinamarciano_photographer/"><i className="fab fa-instagram" style={{fontSize: '140%'}}/></a>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        );
    }
}

export default Footer;