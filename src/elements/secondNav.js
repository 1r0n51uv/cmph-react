import React, {Component} from 'react';

class SecondNav extends Component {
    render() {
        return (
            <div style={{backgroundColor: 'WHITE'}} className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8 pb-30 header-text" style={{marginTop: '5%'}}>
                        <div className="row">

                            <div className="col-md-4">
                                <a href="blog" className="btn btn-default hvr-underline-from-center"><h1 style={{fontFamily: 'Playfair Display'}}>Blog</h1></a>
                            </div>

                            <div className="col-md-4">
                                <a href="gallery" className="btn btn-default hvr-underline-from-center"><h1 style={{fontFamily: 'Playfair Display'}}>Gallery</h1></a>
                            </div>

                            <div className="col-md-4">
                                <a href="about" className="btn btn-default hvr-underline-from-center"><h1 style={{fontFamily: 'Playfair Display'}}>About</h1></a>
                            </div>


                        </div>
                    </div>

                </div>

            </div>
        );
    }
}

export default SecondNav;