import React  from 'react'
import Slider from "../elements/slider";
import About from "./about";
import Gallery from "./gallery";
import SecondNav from "../elements/secondNav";

class Home extends React.Component {

    render() {
        return (
            <div>

                <Slider/>

                <SecondNav/>

                <Gallery/>


                <About/>

            </div>


        );
    }
}

export default Home;