import React, {Component} from 'react';
import {FirestoreCollection} from 'react-firestore'
import Loader from 'react-loader-spinner'
import 'firebase/firestore'

class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: "Recenti"
        }
    }

    changePage(category) {
        this.setState({theme: category});
    }

    render() {
        return (
            <div>
                <section className="gallery-area section-gap" id="gallery">
                    <div className="container">

                        <div className="row justify-content-center">
                            <div className="col-md-8 pb-30 header-text">
                                <h1 className="text-white" style={{fontFamily: 'Playfair Display'}}>I miei scatti</h1>
                                <div className="class-col-md-12">
                                    <div className="btn-group" role="group" aria-label="Basic example">
                                        <button className="btn btn-outline-secondary" onClick={() => {
                                            this.changePage('Recenti')
                                        }}>Recenti</button>
                                        <button className="btn btn-outline-secondary" onClick={() => {
                                            this.changePage('Baby')
                                        }}>Baby</button>
                                        <button className="btn btn-outline-secondary" onClick={() => {
                                            this.changePage('Comunioni')
                                        }}>Comunioni</button>
                                        <button className="btn btn-outline-secondary" onClick={() => {
                                            this.changePage('Ritratti')
                                        }}>Ritratti</button>
                                        <button className="btn btn-outline-secondary" onClick={() => {
                                            this.changePage('Family')
                                        }}>Family</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="container-fluid">

                            <div className="row justify-content-center" style={{marginTop: '2%'}}>



                                <div className="col-md-12">
                                    <FirestoreCollection path="gallery"
                                                         filter={['category', '==', this.state.theme]}
                                                         render={({isLoading, data}) => {
                                                             return isLoading ? (<div className="col-md-12 text-center">
                                                                 <Loader
                                                                     type="Triangle"
                                                                     color="white"
                                                                     height="100"
                                                                     width="100"
                                                                 />
                                                             </div>) : (


                                                                 <div className="row">
                                                                     {
                                                                         data.map(img => (

                                                                             <div className="single-blog col-md-4" key={img.id} style={{marginTop: "5%"}}>
                                                                                 <a key={img.id} href={img.img}><img src={img.img} className="img-fluid" alt=""/></a>

                                                                                 <div className="bottom d-flex justify-content-between align-items-center flex-wrap" style={{borderBottom: '1px solid white'}}>
                                                                                     <h3 style={{fontFamily: 'Playfair Display'}} className="text-white">{img.description}</h3>
                                                                                 </div>
                                                                             </div>

                                                                         ))
                                                                     }
                                                                 </div>
                                                             )

                                                         }}
                                    />
                                </div>



                            </div>

                        </div>
                    </div>






                </section>
            </div>
        );
    }
}

export default Gallery;