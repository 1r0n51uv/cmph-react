import React from 'react'
import parente from './../assets/parente.png'
import MapGL, {Popup} from '@urbica/react-map-gl'

const TOKEN2 = 'pk.eyJ1IjoiMXIwbjUxdXYiLCJhIjoiY2pudmhxazMxMGR5dDNrbzByMGE1dXlxdCJ9.N_tuDxmeFC7tnwIDbw9LnQ';


class About extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                latitude: 40.8134736,
                longitude: 14.5771677,
                zoom: 14,
            },
            marker: {
                latitude: 40.8134736,
                longitude: 14.5771677
            },
            isOpen: true
        };

    }

    render() {


        return(
            <div style={{ backgroundColor: 'white'}}>
                <section className="About-area section-gap" id="about">
                    <div className="container">

                        <div className="row justify-content-center">
                            <div className="col-md-8 pb-30 header-text">
                                <h1 style={{fontFamily: 'Playfair Display'}}>Contact</h1>
                            </div>
                        </div>


                        <div className="row d-flex">

                            <div className="col-md-6">
                                <img className="img-fluid" src={parente} alt=""/>
                                <h1>A LITTLE BIG PARENT</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <p>fDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>

                            <div className="col-md-6">

                                <div className="row">

                                    <div className="col-md-6">
                                        <div className="d-flex flex-column">
                                            <a className="btn" style={{backgroundColor: '#3b5998', color: 'white'}} href="https://www.facebook.com/caterinamarcianophotographer/">
                                                <i className="fab fa-facebook-square" style={{color: 'white'}}/> Facebook
                                            </a>
                                        </div>

                                        <div className="d-flex flex-column" style={{marginTop: "2%"}}>
                                            <a className="btn" style={{backgroundColor: '#e95950', color: 'white'}} href="https://www.instagram.com/caterinamarciano_photographer/">
                                                <i className="fab fa-instagram" style={{color: 'white'}}/> Instagram
                                            </a>
                                        </div>
                                    </div>

                                    <div className="col-md-6 col-sm-12" style={{textAlign: 'left', marginTop: '2%'}}>

                                        <p><i className="far fa-envelope-open" style={{color: 'black'}}/> catemarcianoph@gmail.com</p>
                                        <p><i className="fas fa-phone" style={{color: 'black'}}/> 345-5955921</p>

                                    </div>


                                </div>

                                <MapGL
                                    style={{ width: "100%", height: "400px", marginTop: "2%" }}
                                    mapStyle="mapbox://styles/mapbox/dark-v9"
                                    accessToken={TOKEN2}
                                    {...this.state.viewport}>
                                    {this.state.isOpen && (
                                        <Popup
                                            longitude={this.state.marker.longitude}
                                            latitude={this.state.marker.latitude}
                                            closeOnClick={false}
                                            element={(
                                                <div className="container">
                                                    <div className="row">

                                                        <div className="col-md-12" style={{textAlign: 'center'}}>
                                                            <h4>Ti aspetto allo studio</h4>
                                                            <p><br/>
                                                                <i className="fas fa-map-marker-alt" style={{fontSize: '150%'}}/>
                                                                <a style={{fontSize: '120%'}} href=""> Via Municipio 18, Striano (NA) </a></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            )}
                                        />
                                    )}
                                </MapGL>



                            </div>

                        </div>
                    </div>
                </section>

            </div>
        );

    }

}

export default About;