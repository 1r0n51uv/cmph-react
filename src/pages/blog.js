import React from 'react'
import 'firebase/firestore'
import {FirestoreCollection} from 'react-firestore'
import Loader from 'react-loader-spinner'
import ReactPlayer from 'react-player'



class Blog extends React.Component {

    render() {
        return(

            <div style={{backgroundColor: 'WHITE'}}>
                <section className="blog-area section-gap" id="blog">

                    <div className="container">

                        <div className="row justify-content-center">
                            <div className="col-md-8 pb-30 header-text">
                                <h1>BLOG</h1>
                            </div>
                        </div>



                        <div className="container-fluid">
                            <div className="row">

                                <FirestoreCollection
                                    path="blog"
                                    render={({isLoading, data}) => {
                                        return isLoading ? (
                                            <div className="col-md-12 text-center">
                                                <Loader
                                                    type="Triangle"
                                                    color="black"
                                                    height="100"
                                                    width="100"
                                                />
                                            </div>
                                        ) : (
                                            <div>

                                                <div className="row">
                                                    {
                                                        data.map(post => (
                                                            <div key={post.id} className="single-blog col-lg-4 col-md-4" style={{ marginTop: '2%' }}>

                                                                {
                                                                    post.link !== '' &&
                                                                    <div className="player-wrapper">
                                                                        <ReactPlayer url={post.link} controls={true} width="100%" height="auto" style={{backgroundColor: '#222222'}}/>
                                                                    </div>

                                                                }

                                                                {
                                                                    post.category !== '' &&
                                                                    <img className="f-img img-fluid mx-auto" src={post.img}
                                                                         alt=""/>
                                                                }



                                                                <h4>
                                                                    <a>{post.title}</a>
                                                                </h4>
                                                                <p>
                                                                    {post.body}
                                                                </p>

                                                            </div>

                                                        ))
                                                    }
                                                </div>
                                            </div>

                                        )


                                    }}
                                />

                            </div>
                        </div>



                    </div>

                </section>
            </div>
        );
    }
}

export default Blog;