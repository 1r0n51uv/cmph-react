import React from 'react'
import firebase from "firebase/app";
import { toast } from 'react-toastify';
import CustomUploadButton from "react-firebase-file-uploader/lib/CustomUploadButton";

class Second extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",

            img: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.prepareForm();

    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.updateForm();


    }

    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    handleProgress = progress => this.setState({ progress });
    handleUploadError = error => {
        this.setState({ isUploading: false });
        console.error(error);
    };

    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref("home")
            .child(filename)
            .getDownloadURL()
            .then(url => {
                this.setState({avatarURL: url, img: url});
                this.updateForm();

            });
    };

    prepareForm() {
        firebase.firestore().collection('slider').doc('W8fDD3dcKQIc7lZamVL7').get().then(value => {
            this.setState({
                img: value.data()['path'],
            })
        })
    }

    updateForm() {
        firebase.firestore().collection('slider').doc('W8fDD3dcKQIc7lZamVL7').update(
            {
                "path": this.state.avatarURL,

            }
        ).then(() => {
            toast.success('Seconda immagine modificata con successo!');
        });
    }

    render() {
        return(
            <div>


                <div className="row justify-content-center">

                    <div className="col-md-6">
                        <img src={this.state.img} className="img-fluid" alt=""/>
                        <div className="col-md-12">

                            <div className="col-lg-12 d-flex flex-column" style={{marginTop: "5%"}}>
                                {this.state.isUploading && <p>Progress: {this.state.progress} %</p>}
                                <CustomUploadButton
                                    accept="image/*"
                                    name="second_slider"
                                    storageRef={firebase.storage().ref("home")}
                                    onUploadStart={this.handleUploadStart}
                                    onUploadError={this.handleUploadError}
                                    onUploadSuccess={this.handleUploadSuccess}
                                    onProgress={this.handleProgress}
                                    style={{backgroundColor: 'steelblue', color: 'white', padding: 10, borderRadius: 4}}
                                >
                                    Cambia Foto
                                </CustomUploadButton>


                                <div className="alert-msg"/>
                            </div>

                        </div>
                    </div>



                </div>


            </div>
        );
    }

}

export default Second;