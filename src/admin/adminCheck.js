import React, {Component} from 'react';
import AdminHome from "./adminHome";
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class AdminCheck extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            logged: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit() {
        if (this.state.password === 'a') {
            this.setState({logged: !this.state.logged});
        } else {
            toast.error('Non sei caterina!');
        }
    }


    render() {
        return (
            <div style={{backgroundColor: 'white'}}>

                {
                    this.state.logged ? (<AdminHome logged={this.state.logged}/>) : (<div className="row">

                        <div className="col-md-4">

                        </div>

                        <div className="col-md-4">
                            <section className="contact-area" id="contact">
                                <div className="container-fluid">
                                    <div className="row d-flex justify-content-end align-items-center">
                                        <div className="col-lg-5 col-md-12 contact-left no-padding">
                                            <img className="img-fluid" src="../../assets/img/contact-img.jpg" alt=""/>
                                        </div>
                                        <div className="col-lg-12 col-md-12 contact-right no-padding">
                                            <h1>Sei l'admin?</h1>
                                            <form className="booking-form" id="myForm" onSubmit={this.handleSubmit}>
                                                <div className="row">

                                                    <div className="col-lg-12 d-flex flex-column">
                                                        <input name="password" value={this.state.password}
                                                               onChange={this.handleChange} required
                                                               className="form-control mt-20" type="password"
                                                               style={{color: 'black '}} placeholder="Password"/>
                                                    </div>



                                                    <div className="col-lg-12 d-flex justify-content-center send-btn">
                                                        <button className="primary-btn mt-20 text-uppercase"
                                                                type="submit">Accedi<span className="lnr lnr-arrow-right"/></button>
                                                    </div>

                                                    <div className="alert-msg"/>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>)
                }



                <ToastContainer />
            </div>
        );
    }
}

export default AdminCheck;