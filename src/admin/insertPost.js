import React from 'react'
import firebase from "firebase/app"
import CustomUploadButton from 'react-firebase-file-uploader/lib/CustomUploadButton';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class InsertPost extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            title: "",
            date: "",
            body: "",
            category: "",
            link: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.createPost();
        toast.success('Post Caricato con successo');
    }

    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    handleProgress = progress => this.setState({ progress });
    handleUploadError = error => {
        this.setState({ isUploading: false });
    };

    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref("blog")
            .child(filename)
            .getDownloadURL()
            .then(url => {
                this.setState({avatarURL: url});
            });
    };

    createPost() {
        firebase.firestore().collection('blog').add({
            title: this.state.title,
            date: this.state.date,
            body: this.state.body,
            img: this.state.avatarURL,
            category: this.state.category,
            link: this.state.link
        });
        this.setState({
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            title: "",
            date: "",
            body: "",
            category: "",
            link: ""
        });
    }



    render() {

        return(

            <div style={{backgroundColor: 'white'}} id="insert">

                <div className="container" >

                    <div className="row">

                        <div className="col-md-4" style={{marginTop: '3%'}}>

                            <div className="row justify-content-center">
                                <h2>Preview</h2>
                                <div className="single-blog col-lg-12 col-md-12">

                                    <img className="f-img img-fluid mx-auto" src={this.state.avatarURL} alt=""/>
                                    <h4>
                                        <a>{this.state.title}</a>
                                    </h4>
                                    <p>
                                        {this.state.body}
                                    </p>
                                    <div className="bottom d-flex justify-content-between align-items-center flex-wrap">
                                        <a><i className="fas fa-camera-retro" style={{color: 'gray', fontSize: '120%'}} /><span>{this.state.category}</span></a>
                                        <div className="meta"><i className="far fa-calendar" style={{color: 'gray', fontSize: '120%'}}/>
                                            {' ' + this.state.date}</div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div className="col-md-8">
                            <section className="contact-area" id="contact">
                                <div className="container-fluid">
                                    <div className="row d-flex justify-content-end align-items-center">
                                        <div className="col-lg-5 col-md-12 contact-left no-padding">
                                            <img className="img-fluid" src="../../assets/img/contact-img.jpg" alt=""/>
                                        </div>
                                        <div className="col-lg-12 col-md-12 contact-right no-padding">
                                            <h1>Inserisci un post nel Blog</h1>
                                            <form className="booking-form" id="myForm" onSubmit={this.handleSubmit}>
                                                <div className="row">

                                                    <div className="col-lg-12 d-flex flex-column">
                                                        <input name="title" value={this.state.title}
                                                               onChange={this.handleChange} required
                                                               className="form-control mt-20" type="text"
                                                               style={{color: 'black '}} placeholder="Titolo"/>
                                                    </div>

                                                    <div className="col-lg-12 d-flex flex-column">
                                                        <input name="date" value={this.state.date}
                                                               onChange={this.handleChange} required
                                                               placeholder="Data (13th Aug)" style={{color: 'black '}}
                                                               className="common-input mt-10" type="text"/>
                                                    </div>

                                                    <div className="col-lg-12 d-flex flex-column">
                                                        <input name="category" value={this.state.category}
                                                               onChange={this.handleChange} required
                                                               placeholder="Category" style={{color: 'black '}}
                                                               className="common-input mt-10" type="text"/>
                                                    </div>

                                                    <div className="col-lg-12 flex-column">
                                                    <textarea className="form-control mt-20" value={this.state.body}
                                                              onChange={this.handleChange} required name="body"
                                                              style={{color: 'black '}} placeholder="Descrizione"/>
                                                    </div>

                                                    <div className="col-md-12" style={{marginTop: "2%"}}>
                                                        {this.state.isUploading && <p>Progress: {this.state.progress} %</p>}
                                                        <CustomUploadButton
                                                            accept="image/*"
                                                            name="avatar"
                                                            storageRef={firebase.storage().ref("blog")}
                                                            onUploadStart={this.handleUploadStart}
                                                            onUploadError={this.handleUploadError}
                                                            onUploadSuccess={this.handleUploadSuccess}
                                                            onProgress={this.handleProgress}
                                                            style={{backgroundColor: 'green', color: 'white', padding: 10, borderRadius: 4}}
                                                        >
                                                            Carica Foto
                                                        </CustomUploadButton> <br/> oppure <input name="link" value={this.state.link}
                                                                                            onChange={this.handleChange}
                                                                                            className="form-control mt-20" type="text"
                                                                                            style={{color: 'black '}} placeholder="Incolla Link"/>



                                                    </div>

                                                    <div className="col-lg-12 d-flex justify-content-end send-btn">
                                                        <button className="primary-btn mt-20 text-uppercase"
                                                                type="submit">Inserisci<span className="lnr lnr-arrow-right"/></button>
                                                    </div>

                                                    <div className="alert-msg"/>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                    </div>
                </div>




            </div>
        );

    }


}

export default InsertPost;