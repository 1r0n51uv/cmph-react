import React from 'react'
import { Redirect } from 'react-router'
import InsertPost from "./insertPost";
import ManagePost from "./managePost";
import ManageSlider from "./manageSlider";
import ManageGallery from "./manageGallery";


class AdminHome extends React.Component{
    render() {
        return(
            <div style={{ backgroundColor: 'white'}}>

                {
                    this.props.logged ? (<section className="generic-banner relative">


                        <InsertPost />
                        <hr/>
                        <ManagePost/>
                        <hr/>
                        <ManageSlider/>
                        <hr />
                        <ManageGallery/>

                    </section>) : (<Redirect to="/check" />)
                }


            </div>

        );
    }

}

export default AdminHome;