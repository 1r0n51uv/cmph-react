import React, {Component} from 'react';
import {toast} from "react-toastify";
import firebase from "firebase/app";
import CustomUploadButton from "react-firebase-file-uploader/lib/CustomUploadButton";
import 'react-toastify/dist/ReactToastify.css';
import { Line } from 'rc-progress';


class InsertGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            img: "",
            description: "",
            category: "",
            name: "",
            color: 'green',

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.insertGallery();
        toast.success('Immagine caricata con successo');
    }

    handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
    handleProgress = progress => this.setState({ progress});

    handleUploadError = error => {
        this.setState({ isUploading: false });
        console.error(error);
    };

    handleUploadSuccess = filename => {
        this.setState({ avatar: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref("gallery")
            .child(filename)
            .getDownloadURL()
            .then(url => {
                this.setState({avatarURL: url, name: filename});
            });
    };

    insertGallery() {
        firebase.firestore().collection('gallery').add({
            img: this.state.avatarURL,
            category: this.state.category,
            description: this.state.description,
            name: this.state.name
        });
        this.setState({
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            description: "",
            category: "",
            name: ""
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-left">
                    <div className="col-md-6">

                        <form className="booking-form" id="myForm" onSubmit={this.handleSubmit}>

                            <h3 className="text-white">Inserisci immagine</h3>

                            <div className="col-md-12 d-flex flex-column">
                                <input name="description" value={this.state.description}
                                       onChange={this.handleChange} required
                                       className="form-control mt-20" type="text"
                                       style={{color: 'black '}} placeholder="Descrizione"/>
                            </div>

                            <div className="form-group col-md-12 d-flex flex-column">
                                <select
                                    name="category"
                                    value={this.state.category}
                                    onChange={this.handleChange}
                                    className="form-control" id="exampleFormControlSelect1">
                                    <option>Scegli il tema</option>
                                    <option>Baby</option>
                                    <option>Family</option>
                                    <option>Comunioni</option>
                                    <option>Ritratti</option>
                                    <option>Matrimoni</option>
                                    <option>Recenti</option>
                                </select>
                            </div>


                            <div className="col-lg-12 d-flex flex-column">
                                <CustomUploadButton
                                    accept="image/*"
                                    name="avatar"
                                    storageRef={firebase.storage().ref("gallery")}
                                    onUploadStart={this.handleUploadStart}
                                    onUploadError={this.handleUploadError}
                                    onUploadSuccess={this.handleUploadSuccess}
                                    onProgress={this.handleProgress}
                                    style={{backgroundColor: 'green', color: 'white', padding: 10, borderRadius: 4}}
                                >
                                    Carica Foto
                                </CustomUploadButton>

                                <div className="col-md-12">
                                    {this.state.isUploading &&  <Line percent={this.state.progress} strokeWidth="2" strokeColor={this.state.color} />}
                                </div>

                            </div>

                            <div className="col-lg-12 d-flex flex-column">
                                <button className="btn btn-primary" type="Submit">Inserisci</button>

                            </div>
                        </form>

                    </div>

                    <div className="col-md-5">

                        <img src={this.state.avatarURL} className="img-fluid" alt="" />
                        <h3 className="text-white">{this.state.description}</h3>
                        <span>{this.state.category}</span>
                    </div>

                </div>
            </div>
        );
    }
}

export default InsertGallery;