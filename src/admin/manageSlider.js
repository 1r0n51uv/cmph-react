import React from 'react'
import First from "./slider/first";
import Second from "./slider/second";
import Third from "./slider/third";

class ManageSlider extends React.Component {

    render() {
        return (
            <div style={{ backgroundColor: 'white'}} id="manageS">
                <div className="container">

                    <div className="row justify-content-center">

                        <h2>Gestisci Slider</h2>

                        <First />
                        <hr/>
                        <Second/>
                        <hr/>
                        <Third/>

                    </div>

                </div>

            </div>

        );
    }

}

export default ManageSlider;