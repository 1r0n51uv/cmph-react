import React, {Component} from 'react';
import {FirestoreCollection} from 'react-firestore'
import firebase from 'firebase/app';
import { toast } from 'react-toastify';
import InsertGallery from "./insertGallery";

class ManageGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: "",
            isUploading: false,
            progress: 0,
            avatarURL: "",
            img: "",
            description: "",
            category: ""
        };

    }

    deletePicture(value, value2) {
        firebase.firestore().collection('gallery').doc(value).delete().then(value1 => {
            firebase.storage().ref('gallery').child(value2).delete().then(value1 => {
            }).then(() => {
                toast.success('Immagine eliminata');
            });
        });

    }




    render() {
        return (
            <div id="manageG" style={{backgroundColor: 'black'}}>


                <div className="container">

                    <div className="row justify-content-center">
                        <h1 className="text-white">Gestisci la Galleria</h1>
                        <div className="col-md-12">
                            <InsertGallery/>

                        </div>

                    </div>


                    <FirestoreCollection path="gallery"
                                         render={({isLoading, data}) => {
                                             return isLoading ? (<h1>Caricamento</h1>) : (
                                                 <div>
                                                     <div className="row" style={{marginTop: '2%'}}>

                                                         {
                                                             data.map(img => (

                                                                 <div className="col-md-4" key={img.id}>

                                                                     <img className="img-fluid" style={{marginTop: '2%'}} src={img.img} alt="" />
                                                                     <h3 className="text-white">{img.description}</h3>
                                                                     <br/>
                                                                     <button onClick={() => {
                                                                         this.deletePicture(img.id, img.name);
                                                                     }} style={{marginTop: '2%'}} className="btn btn-danger"><i className="far fa-times-circle" style={{ fontSize: '120%', color: 'white'}}/>Elimina immagine</button>

                                                                 </div>

                                                             ))
                                                         }
                                                     </div>




                                                 </div>
                                             )

                                         }}
                    />

                </div>



            </div>


        );
    }
}

export default ManageGallery;