import React from 'react'
import firebase from 'firebase/app'
import 'firebase/storage';
import 'firebase/firestore';
import {FirestoreCollection} from 'react-firestore'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class ManagePost extends React.Component {


    deletPost(key) {
        firebase.firestore().collection('blog').doc(key).delete();
        toast.error('Post rimosso con successo');
    }

    render() {


        return(
            <div style={{backgroundColor: 'white'}} id="manageP">

                <div className="row justify-content-center">

                    <h2>Gestisci Post</h2>
                </div>

                <FirestoreCollection
                    path="blog"
                    render={({isLoading, data}) => {
                        return isLoading ? (<h1>Caricamento</h1>) : (
                            <div>
                                <div className="row">

                                    <div className="col-md-12">

                                        <table className="table">
                                            <thead>
                                            <tr>
                                                <th scope="col">Delete</th>
                                                <th scope="col">Title</th>
                                                <th scope="col">Body</th>
                                                <th scope="col">Date</th>
                                                <th scope="col">Image</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            {
                                                data.map(post => (
                                                    <tr key={post.id}>
                                                        <td><button className="btn btn-danger" onClick={ () => {
                                                            this.deletPost(post.id)
                                                        }}><i className="far fa-times-circle" style={{ fontSize: '200%', color: 'white'}}/></button></td>
                                                        <td>{post.title}</td>
                                                        <td>{post.body}</td>
                                                        <td>{post.date}</td>
                                                        <td><img src={post.img} className="img-fluid" alt="" width="50%"/></td>
                                                    </tr>
                                                ))
                                            }


                                            </tbody>
                                        </table>


                                    </div>


                                </div>
                            </div>


                        )

                    }}

                />

            </div>
        );


    }


}

export default ManagePost;