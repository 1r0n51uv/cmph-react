import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import firebase from 'firebase/app';
import registerServiceWorker from './registerServiceWorker';


import { FirestoreProvider } from 'react-firestore';

const root = document.getElementById('root');

const config = {
    apiKey: 'AIzaSyAFA3BGE3lXeqpJj_D0g9tkiNwqIEsTpsY',
    authDomain: 'cmph-89134.firebaseapp.com',
    databaseURL: 'https://cmph-89134.firebaseio.com',
    projectId: 'cmph-89134',
    storageBucket: 'cmph-89134.appspot.com',
    messagingSenderId: '194440643279',
    timestampsInSnapshots: true
};

firebase.initializeApp(config);
firebase.firestore().settings({timestampsInSnapshots: true});

ReactDOM.render(<FirestoreProvider firebase={firebase}> <App /> </FirestoreProvider>, root);
registerServiceWorker();
