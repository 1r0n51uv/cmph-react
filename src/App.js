import React, { Component } from 'react';
import './App.css';
import Navbar from "./elements/navbar";
import Footer from "./elements/footer";
import {BrowserRouter as Router, Route} from "react-router-dom";
import Home from "./pages/home";
import AdminHome from "./admin/adminHome";
import Blog from "./pages/blog";
import InsertPost from "./admin/insertPost";
import ManagePost from "./admin/managePost";
import ManageSlider from "./admin/manageSlider";
import AdminCheck from "./admin/adminCheck";
import Gallery from "./pages/gallery";
import About from "./pages/about";

class App extends Component {

    render() {

        return (
            <Router>

                <div style={{fontFamily: 'KoHo', fontSize: '130%', color: 'black'}}>
                    <Navbar />
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/home" component={Home}/>
                    <Route exact path="/admin" component={AdminHome}/>
                    <Route exact path="/blog" component={Blog}/>
                    <Route exact path="/insertPost" component={InsertPost} />
                    <Route exact path="/managePost" component={ManagePost} />
                    <Route exact path="/manageSlider" component={ManageSlider} />
                    <Route exact path="/check" component={AdminCheck} />
                    <Route exact path="/gallery" component={Gallery} />
                    <Route exact path="/gallery2" component={Gallery} />
                    <Route exact path="/about" component={About} />
                    <Footer/>

                </div>
            </Router>

        );
    }
}

export default App;
